import React from 'react';
import './App.scss';
import { AppContainer } from './containers/AppContainer';

const App: React.FC = () => {
  return (
    <AppContainer />
  );
}

export default App;
