import React from 'react';
import { MovieCard } from '../components/MovieCard';

export interface MovieListApi {
  vote_count: number;
  video: boolean;
  poster_path: string;
  id: number;
  backdrop_path: number;
  original_language: string;
  original_title: string;
  genre_ids: number[],
  title: string;
  vote_average: number;
  overview: string;
  release_date: string;
}


// TODO::: need to remove this list

interface MovieListProps {
  movieList: MovieListApi[];
}
export const MovieList: React.FC<MovieListProps> = (props) => {
  const movieResults = props.movieList.map((m) => <MovieCard key={m.id} genres={m.genre_ids} title={m.title} image={m.poster_path} year={m.release_date} />)
  return (
    <div>
      {
        movieResults.length <= 0 ? "No Results Found" : movieResults
      }
    </div>
  )
};