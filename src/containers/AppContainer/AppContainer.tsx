import React, { useEffect, useState } from 'react';
import { TopBar } from '../../components/TopBar/TopBar';
import { $http } from '../../utils/axios';
import { usePolling } from '../../utils/polling';
import { Filter } from '../Filter';
import { MovieList, MovieListApi } from '../MovieList';
import styles from './_app-container.module.scss';


const POLLING = 30;

export const AppContainer: React.FC = () => {
  const [movieList, updateMovieList] = useState([] as MovieListApi[])
  const [searchQuery, onQueryChange] = useState('');
  const [currentPage, updatePage] = useState(1);
  const [noOfPages, updateNoPages] = useState(1);

  useEffect(() => {
    const uri = searchQuery.length > 0 ? '/discover/movie' : '/search/movie';
    $http.get('/discover/movie', {
      params: {
        sort_by: 'popularity.desc',
        include_video: 'false',
        page: currentPage
      }
    }).then(({ data }) => {
      updateMovieList(data.results)
      updateNoPages(data.total_pages)
    }).catch(error => console.log(error))
  }, [currentPage, searchQuery])

  const { status: pollStatus, toggle: togglePolling, update: updatePolling } = usePolling(1, () => {
    console.log('Some polling action ')
  })

  return <div className={styles.container}>
    <div className={styles.main} >
      <button onClick={() => togglePolling()}>{pollStatus ? 'Off' : 'On'} Polling</button>
      <button onClick={() => updatePolling(5)}>5 sec Polling</button>
      <button onClick={() => updatePolling(1)}>1 sec Polling</button>
      <TopBar onQueryChange={onQueryChange} />
      <MovieList movieList={movieList} />
      <div className={styles.pagination}>
        {(new Array(noOfPages)).fill(0).map((n, i) => <div key={'page-' + i} onClick={() => updatePage(i)}>{i}</div>)}
      </div>
    </div>
    <Filter />
  </div>
}