import React, { useState } from 'react';
import { Dropdown } from '../../components/Dropdown';
import { FilterOption } from '../../components/FilterOption';
import styles from './_filter.module.scss';

export const Filter: React.FC = () => {
  const [selectedType, updateSelectedType] = useState(0)
  const [openType, updateOpenType] = useState(false);
  const typeItems = [{
    id: 'movie',
    value: 'Movies'
  }, {
    id: 'shows',
    value: 'TV Shows'
  }]

  const toggleDropdown = (curr: boolean, updater: any) => {
    curr ? updater(false) : updater(true)
  }

  return (<div className={styles.container}>
    <div className={styles.header}>Discover Options</div>
    <div className={styles.filterList}>
      <FilterOption label="type">
        <Dropdown
          items={typeItems}
          selected={selectedType}
          onSelect={updateSelectedType}
          open={openType}
          onToggle={() => toggleDropdown(openType, updateOpenType)} />
      </FilterOption>
      <FilterOption label="genre">
        Dropdown Genre
      </FilterOption>
      <FilterOption label="year">
        Dropdown Range
      </FilterOption>
      <FilterOption label="rating">
        Star Rating
      </FilterOption>
    </div>
  </div>)
}