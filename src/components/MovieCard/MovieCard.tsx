import React from 'react';
import styles from './_movie.module.scss';

interface MovieCardProps {
  image: string;
  title: string;
  genres: number[];
  year: string;
}

export const MovieCard: React.FC<MovieCardProps> = (props) => <div className={styles.container}>
  <img src={props.image} alt="" />
  <div className={styles.title}>
    {props.title}
  </div>
  <div className={styles.subText}>{props.genres.join(', ')}, {props.year}</div>
</div>