import { faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styles from './_dropdown.module.scss';


export interface DropdownItem {
  id: string;
  value: string
}
interface DropdownProps {
  selected: number;
  items: DropdownItem[];
  open: boolean;
  onSelect: any;
  onToggle: any;
}

export const Dropdown: React.FC<DropdownProps> = (props) => {
  const items = props.items.map((item, idx) => <div key={item.id} onClick={() => props.onSelect(idx)} className={idx == props.selected ? styles.active : undefined}>{item.value}</div>)
  const selectedItem = props.items[props.selected];
  return <div className={styles.container} onClick={props.onToggle}>
    <div className={styles.label}>
      <div className={styles.labelText}>
        {selectedItem.value}
      </div>
      <FontAwesomeIcon icon={props.open ? faCaretUp : faCaretDown} />
    </div>
    {props.open ? <div className={styles.itemList}>{items}</div> : null}
  </div>
}