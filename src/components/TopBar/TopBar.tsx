import React from 'react'

interface TopBarProps {
  onQueryChange: any;
}
export const TopBar: React.FC<TopBarProps> = (props) => {
  return <div>
    <input type="text" onChange={props.onQueryChange} />
  </div>
}