import React from 'react';
import styles from './_filter-option.module.scss';

interface FilterOptionProps {
  label: string;
}
export const FilterOption: React.FC<FilterOptionProps> = (props) => {
  return <div className={styles.filterOption}>
    <div className={styles.label}>{props.label}</div>
    <div>
      {props.children}
    </div>
  </div>
}