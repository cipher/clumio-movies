import { useEffect, useState } from 'react';

export const usePolling = (timeInSec: number, cb: any) => {
  const [status, changeStatus] = useState(true)
  const [pollingTime, updatePollingTime] = useState(timeInSec)
  useEffect(() => {
    if (status) {
      const intervalId = setInterval(cb, pollingTime * 1000)
      return () => clearInterval(intervalId)
    }
  }, [status, pollingTime])

  return {
    status,
    toggle() {
      status ? changeStatus(false) : changeStatus(true);
    },
    update(timeInSec: number) {
      updatePollingTime(timeInSec)
    }
  }
}