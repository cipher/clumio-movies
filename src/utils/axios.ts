import axios from 'axios';
import * as Qs from 'qs';

export const $http = axios.create({
  baseURL: 'https://api.themoviedb.org/3/',
  paramsSerializer: (params) => {
    params = {...params, api_key: '3a94078fb34b772a31d9a1348035bed7',
  language: 'en-US',}
    return Qs.stringify(params, {arrayFormat: 'brackets'})
  }
})
